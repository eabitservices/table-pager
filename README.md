# table-pager

A simple pagination for html table. It comes with default search which can be overwritten. It uses bootstrap css for button style.

# Installation and usage

```sh
$ npm install table-pager

In your main entry file 

import TablePager from 'table-pager/dist/index.js'
Vue.use(TablePager)

<template>
  <div>
    <table-pager v-if="users.length > 0" :tableData="users" :searchFunction="pagerQueryFunction">
        <template v-slot:pagertable="{propdata}">
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(item, i) in propdata" :key="i">
                        <td>
                            {{item.name}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </template>
    </table-pager>
  </div>
</template>

<script>
export default {
  name: 'test',
  data () {
    return {
        users: [{name:'Eric'},{name:'Joe'},{name:'Joe'},{name:'Joe'},{name:'Joe'},{name:'Joe'},{name:'Joe'},{name:'Joe'},{name:'Joe'},{name:'Joe'},{name:'Joe'},{name:'Joe'},{name:'Joe'}]
    }
  }
  methods: {
    pagerQueryFunction (data = [],searchKey='') {
        return data.filter(item => {
            return item.name.toLowerCase().includes(searchKey.toLowerCase())
        })
    }
  }
}
</script>

```